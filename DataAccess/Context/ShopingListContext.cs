﻿namespace DataAccess.Context
{
    using System.Data.Entity;
    using Entity;
    using Entity.TokenRenewal;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ShopingListContext : IdentityDbContext<IdentityUser>
    {
        public DbSet<Product> Products { get; set; }

        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public DbSet<AmountDevice> AmountDevices { get; set; }

        public DbSet<ShopeWithPrice> ShopesWithPrices { get; set; }

        public ShopingListContext() : base("ShopingListContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ShopingListContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}

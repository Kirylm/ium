﻿namespace DataAccess.Entity
{
    using Base;
    
    public class AmountDevice : BaseObject
    {
        public string DeviceId { get; set; }

        public int Amount { get; set; }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}

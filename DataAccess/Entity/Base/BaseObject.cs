﻿namespace DataAccess.Entity.Base
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Interface;
    using Newtonsoft.Json;

    public class BaseObject : IBaseObject
    {
        [Key]
        public int Id { get; set; }

        [JsonIgnore]
        public DateTimeOffset? Created { get; set; }

        [JsonIgnore]
        public DateTimeOffset? Modified { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}

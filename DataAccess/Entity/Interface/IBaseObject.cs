﻿namespace DataAccess.Entity.Interface
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public interface IBaseObject
    {
        int Id { get; set; }

        DateTimeOffset? Created { get; set; }

        DateTimeOffset? Modified { get; set; }

        [Timestamp]
        byte[] RowVersion { get; set; }
    }
}

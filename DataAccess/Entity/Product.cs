﻿namespace DataAccess.Entity
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Base;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Newtonsoft.Json;

    public sealed class Product : BaseObject
    {
        public Product()
        {
            this.AmountDevices = new List<AmountDevice>();
        }

        [Required]
        [StringLength(255, ErrorMessage = "Use max 255 characters")]
        [Index]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Required]
        [StringLength(255, ErrorMessage = "Use max 255 characters")]
        [Display(Name = "Shop")]
        public string NameOfShop { get; set; }

        [Required]
        [Display(Name = "Price of product")]
        [Range(0.0, double.MaxValue)]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Amount of product")]
        [Range(0, int.MaxValue)]
        public int Amount { get; set; }

        public string Tag { get; set; }

        public List<ShopeWithPrice> Shopes { get; set; }

        public List<AmountDevice> AmountDevices { get; set; }

        [JsonIgnore]
        public string IdentityUserId { get; set; }

        [JsonIgnore]
        public IdentityUser IdentityUser { get; set; }
    }
}

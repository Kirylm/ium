﻿using DataAccess.Entity.Base;

namespace DataAccess.Entity
{
    public class ShopeWithPrice : BaseObject
    {
        public string ShopName { get; set; }

        public decimal Price { get; set; }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}

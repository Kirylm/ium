﻿namespace DataAccess.Entity.TokenRenewal
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class RefreshToken
    {
        [Key]
        public string Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Subject { get; set; }

        [Required]
        [MaxLength(50)]
        public string ClientId { get; set; }

        public string MobileDeviceId { get; set; }

        public DateTimeOffset IssuedUtc { get; set; }

        public DateTimeOffset ExpiresUtc { get; set; }

        [Required]
        public string ProtectedTicket { get; set; }
    }
}

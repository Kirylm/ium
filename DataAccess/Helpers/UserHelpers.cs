namespace DataAccess.Helpers
{
    using System.Threading.Tasks;
    using Repository;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class UserHelpers
    {
        public static async Task<string> GetUserIdAsync()
        {
            using (var authRepository = new AuthRepository())
            {
                var userName = UserInfo.GetUserName();
                return await authRepository.FindUserByNameAsync(userName);
            }
        }

        public static string GetUserId()
        {
            using (var authRepository = new AuthRepository())
            {
                var userName = UserInfo.GetUserName();
                return authRepository.FindUserByName(userName);
            }
        }
    }
}
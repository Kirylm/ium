﻿namespace DataAccess.Helpers
{
    using System.Security.Claims;

    public static class UserInfo
    {
        public static string GetUserName()
        {
            if (!ClaimsPrincipal.Current.Identity.IsAuthenticated) return "Anonymous";
            var identity = (ClaimsIdentity)ClaimsPrincipal.Current.Identity;
            var stringName = "Anonymous";
            foreach (var claim in identity.Claims)
            {
                if (claim.Type == ClaimTypes.Name)
                    stringName = claim.Value;
            }

            return stringName;
        }

        public static string GetUserMobileDevie()
        {
            if (!ClaimsPrincipal.Current.Identity.IsAuthenticated) return "Anonymous";
            var identity = (ClaimsIdentity)ClaimsPrincipal.Current.Identity;
            var mobileDevice = "Anonymous";
            foreach (var claim in identity.Claims)
            {
                if (claim.Type == ClaimTypes.UserData)
                    mobileDevice = claim.Value;
            }

            return mobileDevice;
        }
    }
}

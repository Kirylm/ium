namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changes : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Products", new[] { "IdentityUser_Id" });
            DropColumn("dbo.Products", "IdentityUserId");
            RenameColumn(table: "dbo.Products", name: "IdentityUser_Id", newName: "IdentityUserId");
            AlterColumn("dbo.Products", "IdentityUserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Products", "IdentityUserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Products", new[] { "IdentityUserId" });
            AlterColumn("dbo.Products", "IdentityUserId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Products", name: "IdentityUserId", newName: "IdentityUser_Id");
            AddColumn("dbo.Products", "IdentityUserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "IdentityUser_Id");
        }
    }
}

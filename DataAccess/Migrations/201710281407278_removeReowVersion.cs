namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeReowVersion : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Products", "RowVersion");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "RowVersion", c => c.Binary());
        }
    }
}

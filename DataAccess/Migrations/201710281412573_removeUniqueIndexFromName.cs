namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeUniqueIndexFromName : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Products", new[] { "ProductName" });
            CreateIndex("dbo.Products", "ProductName");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Products", new[] { "ProductName" });
            CreateIndex("dbo.Products", "ProductName", unique: true);
        }
    }
}

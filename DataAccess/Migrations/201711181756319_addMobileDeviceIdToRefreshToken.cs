namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMobileDeviceIdToRefreshToken : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RefreshTokens", "MobileDeviceId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RefreshTokens", "MobileDeviceId");
        }
    }
}

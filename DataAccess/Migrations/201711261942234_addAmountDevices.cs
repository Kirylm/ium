namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAmountDevices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AmountDevices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeviceId = c.String(),
                        Amount = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        Modified = c.DateTimeOffset(precision: 7),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AmountDevices", "ProductId", "dbo.Products");
            DropIndex("dbo.AmountDevices", new[] { "ProductId" });
            DropTable("dbo.AmountDevices");
        }
    }
}

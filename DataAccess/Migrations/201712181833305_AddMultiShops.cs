namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMultiShops : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ShopeWithPrices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShopName = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductId = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        Modified = c.DateTimeOffset(precision: 7),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            AddColumn("dbo.Products", "Tag", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShopeWithPrices", "ProductId", "dbo.Products");
            DropIndex("dbo.ShopeWithPrices", new[] { "ProductId" });
            DropColumn("dbo.Products", "Tag");
            DropTable("dbo.ShopeWithPrices");
        }
    }
}

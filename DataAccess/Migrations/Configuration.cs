namespace DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    using Entity.TokenRenewal;

    internal sealed class Configuration : DbMigrationsConfiguration<Context.ShopingListContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Context.ShopingListContext context)
        {
            var client = new Client
            {
                Id = "IUMApp",
                Secret = Helper.GetHash("Secret"),
                Name = "IUMApp",
                ApplicationType = ApplicationTypes.JavaScript,
                Active = true,
                RefreshTokenLifeTime = 1440000,
                AllowedOrigin = "*"
            };
            context.Clients.AddOrUpdate(client);
        }
    }
}

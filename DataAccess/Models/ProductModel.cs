﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Models
{
    public class ProductModel
    {
        public int? Id { get; set; }

        [Required]
        [StringLength(255, ErrorMessage = "Use max 255 characters")]
        [Index]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Required]
        [StringLength(255, ErrorMessage = "Use max 255 characters")]
        [Display(Name = "Shop")]
        public string NameOfShop { get; set; }

        [Required]
        [Display(Name = "Price of product")]
        [Range(0.0, double.MaxValue)]
        public decimal Price { get; set; }

        public string Tag { get; set; }

        [Required]
        [Display(Name = "Amount of product")]
        [Range(0, int.MaxValue)]
        public int YourAmount { get; set; }

        public int? OtherDevices { get; set; }

        public IEnumerable<ShopeWithPriceModel> Shopes { get; set; }
    }
}

﻿namespace DataAccess.Models
{
    public class ShopeWithPriceModel
    {
        public string ShopName { get; set; }

        public decimal Price { get; set; }
    }
}

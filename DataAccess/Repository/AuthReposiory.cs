﻿using System.Data.Entity;

namespace DataAccess.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Context;
    using Entity.TokenRenewal;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;

    public class AuthRepository : IDisposable
    {
        private readonly ShopingListContext _ctx;

        private readonly UserManager<IdentityUser> _userManager;

        public AuthRepository()
        {
            this._ctx = new ShopingListContext();
            this._userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(this._ctx));
        }

        public async Task<IdentityResult> RegisterUserAsync(UserModel userModel)
        {
            var user = new IdentityUser
            {
                UserName = userModel.UserName
            };

            var result = await this._userManager.CreateAsync(user, userModel.Password);

            return result;
        }

        public async Task<IdentityUser> FindUserAsync(string userName, string password)
        {
            var user = await this._userManager.FindAsync(userName, password);
            return user;
        }

        public async Task<string> FindUserByNameAsync(string userName)
        {
            var user = await this._userManager.FindByNameAsync(userName);
            return user.Id;
        }

        public string FindUserByName(string userName)
        {
            var user = this._userManager.FindByName(userName);
            return user.Id;
        }

        public Client FindClient(string clientId)
        {
            var client = this._ctx.Clients.Find(clientId);

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {
            var existingTokens = await this._ctx.RefreshTokens.Where(r =>
            r.Subject == token.Subject
            && r.ClientId == token.ClientId
            && r.MobileDeviceId == token.MobileDeviceId).ToListAsync();

            foreach (var existingToken in existingTokens)
            {
                await this.RemoveRefreshToken(existingToken);  
            }
            
            this._ctx.RefreshTokens.Add(token);

            return await this._ctx.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await this._ctx.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken == null) return false;
            this._ctx.RefreshTokens.Remove(refreshToken);
            return await this._ctx.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            this._ctx.RefreshTokens.Remove(refreshToken);
            return await this._ctx.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await this._ctx.RefreshTokens.FindAsync(refreshTokenId);

            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return this._ctx.RefreshTokens.ToList();
        }

        public void Dispose()
        {
            this._ctx.Dispose();
            this._userManager.Dispose();
        }
    }
}

﻿namespace DataAccess.Repository
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Entity.Interface;


    public interface IRepository<T> : IDisposable where T : IBaseObject
    {
        Task<T> FindByIdAsync(int id);

        IQueryable<T> GetAll();

        Task<T> AddAsync(T entity);

        Task<int> UpdateAsync(T entity);

        Task<int> DeleteAsync(int id);
    }
}

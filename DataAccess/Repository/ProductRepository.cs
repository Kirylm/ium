﻿using DataAccess.Entity.Interface;

namespace DataAccess.Repository
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using Entity;
    using Helpers;
    using Models;

    public class ProductRepository : Repository<Product>
    {
        public async Task<ProductModel> CreateProduct(ProductModel productModel)
        {
            var userId = await UserHelpers.GetUserIdAsync();
            var mobileDevice = UserInfo.GetUserMobileDevie();

            var product = new Product()
            {
                IdentityUserId = userId,
                NameOfShop = productModel.NameOfShop,
                ProductName = productModel.ProductName,
                Price = productModel.Price,
                Tag = productModel.Tag,
                Amount = 0
            };

            product = await base.AddAsync(product);

            await CreateAmountDevice(productModel, product, mobileDevice);

            await CreateShopWithPrice(productModel, product);

            productModel.Id = product.Id;

            return productModel;
        }

        private static async Task CreateShopWithPrice(ProductModel productModel, IBaseObject product)
        {
            using (var shopWithPriceRepository = new Repository<ShopeWithPrice>())
            {
                if (productModel.Shopes != null)
                {
                    foreach (var shop in productModel.Shopes)
                    {
                        var shopWithPrice = new ShopeWithPrice()
                        {
                            ShopName = shop.ShopName,
                            Price = shop.Price,
                            ProductId = product.Id
                        };

                        await shopWithPriceRepository.AddAsync(shopWithPrice);
                    }
                }
            }
        }

        private static async Task CreateAmountDevice(ProductModel productModel, 
            IBaseObject product, string mobileDevice)
        {
            using (var amountRepository = new Repository<AmountDevice>())
            {
                var amountDevice = new AmountDevice()
                {
                    Amount = productModel.YourAmount,
                    ProductId = product.Id,
                    DeviceId = mobileDevice
                };

                await amountRepository.AddAsync(amountDevice);
            }
        }

        public async Task<IEnumerable<ProductModel>> GetAllProductModelsAsync()
        {
            var mobileDevice = UserInfo.GetUserMobileDevie();

            var products = await this.GetAll().Include("Shopes").Include("AmountDevices").ToListAsync();
            
            var productModels = from product in products
                                let yourDevice = product.AmountDevices
                                .Where(m => m.DeviceId == mobileDevice)
                                    .Select(m => m.Amount)
                                    .Sum()
                                let others = product
                                    .AmountDevices
                                    .Where(m => m.DeviceId != mobileDevice)
                                    .Select(m => m.Amount)
                                    .Sum()

                                let shopes = product.Shopes.Select(m => new ShopeWithPriceModel()
                                {
                                    ShopName = m.ShopName,
                                    Price = m.Price
                                })

                                select new ProductModel()
                                {
                                    Id = product.Id,
                                    NameOfShop = product.NameOfShop,
                                    OtherDevices = others,
                                    Price = product.Price,
                                    ProductName = product.ProductName,
                                    YourAmount = yourDevice,
                                    Shopes = shopes,
                                    Tag = product.Tag
                                };

            return productModels;
        }

        public async Task UpdateProduct(int productId, int amount)
        {
            var mobileDevice = UserInfo.GetUserMobileDevie();

            var element = this.GetAll().Where(m => m.Id == productId)
                .SelectMany(m => m.AmountDevices)
                .FirstOrDefault(m => m.DeviceId == mobileDevice);

            using (var amountRepository = new Repository<AmountDevice>())
            {
                if (element == null)
                {
                    var amountDevice = new AmountDevice()
                    {
                        Amount = amount,
                        ProductId = productId,
                        DeviceId = mobileDevice
                    };

                    await amountRepository.AddAsync(amountDevice);
                }
                else
                {
                    element.Amount = amount;
                    await amountRepository.UpdateAsync(element);
                }
            }
        }

        public override IQueryable<Product> GetAll()
        {
            var userId = UserHelpers.GetUserId();
            return base.GetAll().Where(m => m.IdentityUserId == userId);
        }

        public override async Task<Product> AddAsync(Product entity)
        {
            var userId = await UserHelpers.GetUserIdAsync();
            entity.IdentityUserId = userId;
            return await base.AddAsync(entity);
        }
    }
}

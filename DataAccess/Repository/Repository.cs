﻿namespace DataAccess.Repository
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using Context;
    using Entity.Base;

    public class Repository<T> : IRepository<T> where T : BaseObject
    {
        private readonly ShopingListContext _ctx;

        public Repository()
        {
            this._ctx = new ShopingListContext();
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            this._ctx.Set<T>().Add(entity);
            await this._ctx.SaveChangesAsync();
            return entity;
        }

        public virtual async Task<int> DeleteAsync(int id)
        {
            var entity = await this.FindByIdAsync(id);
            this._ctx.Set<T>().Remove(entity);
            return await this._ctx.SaveChangesAsync();
        }

        public virtual async Task<T> FindByIdAsync(int id)
        {
            return await this._ctx.Set<T>().FirstAsync(m => m.Id == id);
        }

        public virtual IQueryable<T> GetAll()
        {
            return this._ctx.Set<T>();
        }

        public virtual async Task<int> UpdateAsync(T entity)
        {
            if (entity == null)
            {
                return 0;
            }

            var existing = await this.FindByIdAsync(entity.Id);
            if (existing != null)
            {
                this._ctx.Entry(existing).CurrentValues.SetValues(entity);
            }

            return await this._ctx.SaveChangesAsync();
        }

        public void Dispose()
        {
            this._ctx.Dispose();
        }

        protected void UpdateWithCurrentContext(T entity, bool created, bool modified)
        {
            var now = DateTimeOffset.UtcNow;

            if (created)
            {
                entity.Created = now;
                entity.Modified = now;
            }

            if (modified)
            {
                entity.Modified = now;
            }
        }
    }
}

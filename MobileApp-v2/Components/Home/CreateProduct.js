import React, { Component } from 'react'
import { StyleSheet, 
    View, 
    Text, 
    Button, 
    TextInput,
    Keyboard,
    Picker,
    FlatList,
    ScrollView
} from 'react-native';
import { Card } from 'react-native-elements'
import { NumericTextChanged, FloatTextChanged } from '../../Helpers/NumericInputHelper'
import Shopes from './Shopes'

export default class CreateProduct extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {name: '', 
            amount: '',
            price: '',
            shop: '',
            tag: 'Home',
            shopes: [],
            onClick: params.onClick};
    }

    onAmountChanged(text) {
        NumericTextChanged(text, (amount) => this.setState({amount}));
    }

    onPriceChanged(text) {
        FloatTextChanged(text, (price) => this.setState({price}));
    }

    addShop() {
        let element = {
            ShopName : '',
            Price : '',
            shopName : '',
            price : ''
        };
        this.setState({shopes:[...this.state.shopes, element]});
    }

    changeNameOfElement(index, name) {
        let { shopes } = this.state;
        shopes[index].ShopName = name;
        shopes[index].shopName = name;
        console.log(shopes[index]);
        this.setState({shopes});  
    }

    changePriceOfElement(index, text) {
        let { shopes } = this.state;
        FloatTextChanged(text, (price) => {
            shopes[index].Price = price;
            shopes[index].price = price;
        });
        console.log(shopes[index]);
        console.log(this.state);
        this.setState({shopes});  
    }

    async submitProduct() {
        Keyboard.dismiss();

        console.log(this.state.amount);

        const product = {
          "ProductName" : this.state.name,
          "NameOfShop" : this.state.shop,
          "Price" : parseFloat(this.state.price),
          "YourAmount" : parseInt(this.state.amount),
          "Tag" : this.state.tag,
          "Shopes" : this.state.shopes
        };

        await this.state.onClick(product);
    }

    render() {
        return(
            <ScrollView>
                <Card>
                    <TextInput 
                        placeholder = "Name of product"
                        value = {this.state.name}
                        onChangeText = {(name)=> this.setState({name})}
                        returnKeyLabel = "Next"
                        ref = {(input) => this.nameInput = input}
                        onSubmitEditing = {() => {this.shopInput.focus()}}
                    />
                    <TextInput 
                        placeholder = "Name of shop"
                        value = {this.state.shop}
                        onChangeText = {(shop) => this.setState({shop})}
                        returnKeyLabel = "Next"
                        ref = {(input) => this.shopInput = input}
                        onSubmitEditing = {() => {this.amountInput.focus()}}
                    />
                    <TextInput 
                        placeholder = "Price"
                        value = {this.state.price}
                        keyboardType = 'numeric'
                        onChangeText = {(price) => this.onPriceChanged(price)}
                        returnKeyLabel = "Next"
                        ref = {(input) => this.priceInput = input}
                    />
                    {
                        this.state.shopes.map((item, index) => {
                            return (<View key = {index}>
                                <TextInput 
                                    placeholder = "Name of shop"
                                    value = {item.NameOfShop}
                                    onChangeText = {(shop) =>  
                                        {
                                            this.changeNameOfElement(index, shop);
                                            console.log(item.NameOfShop);
                                        }
                                    } 
                                    returnKeyLabel = "Next"
                                />
                                <TextInput 
                                    placeholder = "Price"
                                    value = {this.state.shopes[index].Price}
                                    keyboardType = 'numeric'
                                    onChangeText = {(text) => this.changePriceOfElement(index, text) }
                                    returnKeyLabel = "Next"
                                />
                            </View>)
                        })
                    }
                    <TextInput 
                        placeholder = "Amount"
                        value = {this.state.amount}
                        keyboardType = 'numeric'
                        onChangeText = {(amount) => this.onAmountChanged(amount)}
                        returnKeyLabel = "Next"
                        ref = {(input) => this.amountInput = input}
                        onSubmitEditing = {() => {this.priceInput.focus()}}
                    />
                    <Picker
                        selectedValue={this.state.tag}
                        onValueChange={(tag, itemIndex) => {console.log(tag); this.setState({tag})}}>
                        <Picker.Item label="Home" value="Home" />
                        <Picker.Item label="Work" value="Work" />
                    </Picker>
                    <Button
                        title="Add shop"
                        onPress = {() => this.addShop()}
                    />
                    <Button 
                        title="Submit" 
                        backgroundColor="#4286f4"
                        disabled={
                            this.state.name.length === 0 ||
                            this.state.shop.length === 0 ||
                            this.state.amount.length === 0 ||
                            this.state.price.length === 0 ||
                            this.state.tag.length === 0 
                        }

                        onPress = {() => this.submitProduct()}
                    />
                </Card>
            </ScrollView>
        );
    }
}
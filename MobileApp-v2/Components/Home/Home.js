/**
 * Created by Kiryl on 10/22/2017.
 */
import React, { Component } from 'react';
import {StyleSheet, 
    View, 
    Text, 
    Button, 
    ActivityIndicator,
    FlatList,
    ScrollView,
    Keyboard,
    Alert,
    RefreshControl,
    BackHandler
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import {ListItem} from 'react-native-elements';
import AuthorizationService from '../../Authorization/AuthorizationService';
import RestCaller from '../../RestCaller/RestCaller';
import ProductCard from './ProductCard';
import CreateProduct from './CreateProduct';
import GetDeviceIdWrapper from './../../Helpers/GetDeviceIdWrapper';
import NetInfoWrapper from './../../Helpers/NetInfoWrapper';

export default class Home extends Component {
    constructor(props) {
        super(props);
        console.log(this.props.navigation);
        const {navigate} = this.props.navigation;

        this.restCaller = new RestCaller(() => navigate("LoginUser"));
    
        this.state = {isLoading: true, navigate: navigate, 
            refreshing: false, isConnected: false};
    }
    
    static navigationOptions(navigation) {
        console.log(navigation);
        const {navigate} = navigation.navigation;
        
        return {
            title: 'Home',
            headerRight: 
            <View>
            <Button
                title = "Sign Out" 
                backgroundColor = "#4286f4"
                onPress = {async () => await Home.logOutUser(() => navigate("LoginUser"))}
            />
            </View>
        }
    };

    async componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            BackHandler.exitApp();
        });
        const isConnected = await NetInfoWrapper.isConnected();
        this.setState({isConnected});
        await this.getProducts();
        NetInfoWrapper.addOnChangeEventListner((isConnected) =>
            this.setState({isConnected}));
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
        NetInfoWrapper.removeEventListner();
    }

    async getProducts() {
        const data = await this.restCaller.getProducts();
        this.setState({
            products: data,
            isLoading: false
        });
    }

    async onRefresh() {
        this.setState({refreshing : true});
        await this.getProducts();
        this.setState({refreshing : false});
    }

    static async logOutUser(onLogout) {
        const isConnected = await NetInfoWrapper.isConnected();
        if (isConnected)
        {
            await AuthorizationService.logOut();
            onLogout();
        }
        else 
        {
            alert("You don't have internet connection");
        }
    }

    async clearQueue() {
        await this.restCaller.clearQueue();
        await this.getProducts();    
    }

    async patchProduct(increaseModel) {
        await this.restCaller.updateAmount(increaseModel);
        this.state.navigate("AppView");
    }

    deleteProductAlert(id) {
        Alert.alert("Are you sure?", "You want to delete this product?", 
        [
            {
                text: "Ok",
                onPress: async () => {await this.deleteProduct(id)}
            },
            {
                text: "Cancel",
                style: 'cancel'
            }
        ])
    }

    async deleteProduct(id) {
        await this.restCaller.deleteProduct(id);
        this.state.navigate("AppView");
        Keyboard.dismiss();
    }

    async createProduct(product) {
        await this.restCaller.createProduct(product);
        this.state.navigate("AppView");
    }
    
    render() {
        const {navigate} = this.props.navigation;
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <View style = {styles.container}>
                <View style = {styles.buttons}>
                    <Button 
                        title = "Resync"
                        onPress = {async () => await this.clearQueue()}
                        disabled = {!this.state.isConnected}
                    />
                </View>
                <View style = {styles.buttons}>
                    <Button
                        title="Create new product" 
                        backgroundColor="#00cc00"
                        onPress={() => navigate("CreateProduct", { 
                            onClick : (product) => this.createProduct(product)
                        })}
                    />
                </View>
                <ScrollView>
                    <FlatList
                    refreshControl = {
                        <RefreshControl 
                            refreshing = {this.state.refreshing}
                            onRefresh = {() => this.onRefresh()}
                        />
                    }
                    data = {this.state.products}
                    renderItem = {({item}) => 
                       <ListItem                       
                            title = {`Product name: ${item.productName}`}
                            subtitle = {`Shop : ${item.nameOfShop}, Amount : ${item.yourAmount 
                                + item.otherDevices}, Price : ${item.price}, Tag : ${item.tag}`}
                            
                            onPress = {() => navigate("ProductCard", {
                                onClick : async (increase, increaseModel) 
                                    => await this.patchProduct(increase, increaseModel),
                                onDeleteClick : (id) => this.deleteProductAlert(id),
                                element : item
                            })}
                       />
                    }
                    keyExtractor={(item, index) => item.id}
                    />
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    buttons: {
        height : 35,
        justifyContent: 'space-between',
        margin : 10
    }
});
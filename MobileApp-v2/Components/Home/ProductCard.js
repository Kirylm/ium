import React, { Component } from 'react';
import { ScrollView, StyleSheet, View, Text, Button, TextInput, Keyboard } from 'react-native';
import { Card } from 'react-native-elements'
import { NumericTextChanged } from '../../Helpers/NumericInputHelper'
import NetInfoWrapper from '../../Helpers/NetInfoWrapper';

export default class ProductCard extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            myNumber: '', 
            onClick: params.onClick,  
            onDeleteClick: params.onDeleteClick,
            isConnected: true
        };
    }

    async componentWillMount() {
        const isConnected = await NetInfoWrapper.isConnected();
        this.setState({isConnected});
        NetInfoWrapper.addOnChangeEventListner((isConnected) =>
            this.setState({isConnected}));
    };

    componentWillUnmount() {
        NetInfoWrapper.removeEventListner();
    }

    onChanged(text) {
        NumericTextChanged(text, (myNumber) => this.setState({myNumber}));
    }

    buttonClicked(isIncreasing) {
        const { params } = this.props.navigation.state;
        const { element } = this.props.navigation.state.params;
        Keyboard.dismiss();
        if (isIncreasing) {
            this.state.onClick({ Id : params.element.id, 
                Amount : parseInt(element.yourAmount) + parseInt(this.state.myNumber)});
        }
        else {
            this.state.onClick({ Id : params.element.id, 
                Amount : parseInt(element.yourAmount) - parseInt(this.state.myNumber)});
        }
        this.setState({ myNumber: ''});
    }

    render() {
        const { element } = this.props.navigation.state.params;
        element.amount = element.yourAmount + element.otherDevices
        return (
            <ScrollView>
                <Card>
                    <Text>Name: {element.productName}</Text>
                    <Text>Name of shop: {element.nameOfShop}</Text>
                    <Text>Price: {element.price}</Text>
                    {
                        element.shopes.map((item, index) => {
                            return (<View key = {index}>
                                <Text>Name of shop: {item.shopName}</Text>
                                <Text>Price: {item.price}</Text>
                            </View>)
                        })
                    }
                    <Text>Tag: {element.tag}</Text>
                    <Text>Amount: {element.amount}</Text>
                    <TextInput
                        placeholder = "Change Amount"
                        keyboardType = 'numeric'
                        onChangeText = {(text)=> this.onChanged(text)}
                        value = {this.state.myNumber}
                        maxLength = {10}     
                    />
                    <View style = {styles.container}>
                        <Button
                                title = "Increase value" 
                                backgroundColor = "#4286f4"
                                onPress = {() => this.buttonClicked(true)}
                                disabled = {this.state.myNumber.length === 0 || parseInt(this.state.myNumber) === 0}
                        />
                        <Button
                                title = "Decrease value" 
                                backgroundColor = "#4286f4"
                                onPress = {() => this.buttonClicked(false)}
                                disabled = {this.state.myNumber.length === 0 || 
                                    parseInt(this.state.myNumber) === 0 ||
                                    parseInt(this.state.myNumber) > element.amount ||
                                    (
                                        !this.state.isConnected 
                                        && parseInt(this.state.myNumber) > element.yourAmount
                                    )
                                }
                        />
                    </View>
                    <View style = {styles.deleteButton}>
                        <Button
                            title = "Delete" 
                            backgroundColor = "#4286f4"
                            onPress = {() => this.state.onDeleteClick(element.id)}
                        />
                    </View>
                </Card>
            </ScrollView>    
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    deleteButton: {
        margin : 10
    }
});
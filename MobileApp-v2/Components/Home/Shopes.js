import React, { Component } from 'react';
import { TextInput, View } from 'react-native';
import { FloatTextChanged } from '../../Helpers/NumericInputHelper';

export default class Shopes extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let element = this.props.element;
        return (
        <View>
            <TextInput 
                placeholder = "Name of shop"
                value = {element.NameOfShop}
                onChangeText = {(shop) =>  
                    {
                        element.NameOfShop = shop;
                        console.log(element.NameOfShop);
                    }
                } 
                returnKeyLabel = "Next"
            />
            <TextInput 
                placeholder = "Price"
                value = {element.Price}
                keyboardType = 'numeric'
                onChangeText = {(text) =>  FloatTextChanged(text, (price) => element.Price = price)}
                returnKeyLabel = "Next"
            />
        </View>
        );
    }
}
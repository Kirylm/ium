/**
 * Created by Kiryl on 10/22/2017.
 */
import React, { Component } from 'react';
import { 
    StyleSheet,
    View,
    TextInput, 
    Keyboard, 
    BackHandler
} from 'react-native';
import { Card, Button } from 'react-native-elements';
import AuthorizationService from './../../Authorization/AuthorizationService';
import NetInfoWrapper from './../../Helpers/NetInfoWrapper';


export default class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {userName: '', password: ''};
    }

    async componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            BackHandler.exitApp();
        });

        NetInfoWrapper.ifNotConnectedClosed();
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    async onButtonPressedFunction(navigate) {
        const user = {
            userName : this.state.userName,
            password : this.state.password
        };
        await AuthorizationService.loginUser(user, navigate);
        this.clearFields();
    }

    clearFields() {
        this.userNameInput.clear();
        this.passwordInput.clear();
        Keyboard.dismiss();
    }

    setUserName(text) {
        const isButtonDisabled = this.isButtonDisabled();
        this.setState({userName: text});
        this.setState({disableButton: isButtonDisabled});
    }

    setPassword(text) {
        const isButtonDisabled = this.isButtonDisabled();
        this.setState({password: text});
        this.setState({disableButton: isButtonDisabled});
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Card>
                    <TextInput
                        placeholder = "User name"
                        returnKeyLabel = 'Next'
                        style = {styles.input}
                        ref = {(input) => this.userNameInput = input}
                        onSubmitEditing = {() => {this.passwordInput.focus()}}
                        autoCapitalize = "none"
                        onChangeText = {(userName) =>  {this.setState({userName})}}
                        autoCorrect = {false}
                        value = {this.state.userName}
                    />
                    <TextInput
                        placeholder = "Password"
                        returnKeyLabel = "Next"
                        secureTextEntry
                        style = {styles.input}
                        ref = {(input) => this.passwordInput = input}
                        onChangeText = {(password) => {this.setState({password})}}
                        autoCapitalize = "none"
                        autoCorrect = {false}
                        value = {this.state.password}
                    />
                    <View style = {styles.buttons}>
                        <Button title = "Login"
                            backgroundColor = "#4286f4"
                            disabled = {
                                this.state.password.length === 0
                                || this.state.userName.length === 0
                            } 
                            onPress = {() => this.onButtonPressedFunction(navigate)}
                        />
                    </View>
                    <View>
                        <Button 
                            title = "Sign Up" 
                            backgroundColor = "#4286f4"
                            onPress = {() => navigate("Register")}
                        />
                    </View>
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    input: {
        height: 40,
        width: 250,
        backgroundColor: 'white',
        marginBottom: 15,
        paddingHorizontal: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    buttons : {
        marginBottom : 15
    }
});
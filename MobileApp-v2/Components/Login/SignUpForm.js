/**
 * Created by Kiryl on 10/22/2017.
 */
import React, { Component } from 'react';
import { StyleSheet, View, TextInput } from 'react-native';
import { Card, Button } from 'react-native-elements';
import AuthorizationService from './../../Authorization/AuthorizationService'

export default class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {userName: '', password: '', confirmedPassword: ''};
    }

    async onButtonPressedFunction(navigate) {
        if (this.state.confirmedPassword !== this.state.password) {
            alert("Password and Confirm Password are diffrent");
            this.clearFields();
            return;
        }
        
        const user = {
            UserName : this.state.userName,
            Password : this.state.password,
            ConfirmPassword : this.state.confirmedPassword
        };

        const data = await AuthorizationService.registerUser(user);
        if (data) {
            const login = {
                userName : this.state.userName,
                password : this.state.password
            }
            
            await AuthorizationService.loginUser(login, navigate);
        }

        this.clearFields();
    }

    async componentWillMount() {
        NetInfoWrapper.ifNotConnectedClosed();
    };

    clearFields() {
        this.userNameInput.clear();
        this.confirmInput.clear();
        this.passwordInput.clear();
        this.userNameInput.focus();
        this.setState({userName: '', password: '', confirmedPassword: ''});
    }

    // setUserName(text) {
    //     this.setState({userName: text});
    //     const isButtonDisabled = this.isButtonDisabled();
    //     this.setState({disableButton: isButtonDisabled});
    // }

    // setPassword(text) {
    //     this.setState({password: text});
    //     const isButtonDisabled = this.isButtonDisabled();
    //     this.setState({disableButton: isButtonDisabled});
    // }

    // setConfirm(text) {
    //     this.setState({confirmedPassword: text});
    //     const isButtonDisabled = this.isButtonDisabled();
    //     this.setState({disableButton: isButtonDisabled});
    // }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <Card>
                    <TextInput
                        placeholder = "User name"
                        returnKeyLabel = 'Next'
                        style = {styles.input}
                        onSubmitEditing = {() => {this.passwordInput.focus()}}
                        ref = {(input) => this.userNameInput = input}
                        autoCapitalize = "none"
                        onChangeText = {(userName) =>  {this.setState({userName})}}
                        autoCorrect = {false}
                        value = {this.state.userName}
                    />

                    <TextInput
                        placeholder = "Password"
                        returnKeyLabel = 'Next'
                        secureTextEntry
                        style = {styles.input}
                        onSubmitEditing = {() => {this.confirmInput.focus()}}
                        ref = {(input) => this.passwordInput = input}
                        onChangeText = {(password) => {this.setState({password})}}
                        autoCapitalize = "none"
                        autoCorrect = {false}
                        value = {this.state.password}
                    />

                    <TextInput
                        placeholder = "Confirm password"
                        returnKeyLabel = 'Next'
                        secureTextEntry
                        style={styles.input}
                        ref={(input) => this.confirmInput = input}
                        onChangeText = {(confirmedPassword) => {this.setState({confirmedPassword})}}
                        autoCapitalize = "none"
                        autoCorrect = {false}
                        value = {this.state.confirmedPassword}
                    />

                    <Button
                        title = "Create Account" 
                        backgroundColor = "#4286f4"
                        disabled = {
                            this.state.userName.length === 0 
                            || this.state.password.length <= 5
                            || this.state.confirmedPassword.length <= 5
                            || this.state.password !== this.state.confirmedPassword
                        }
                        onPress = {() => this.onButtonPressedFunction(navigate)}
                    />
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    input: {
        height: 40,
        width: 250,
        backgroundColor: 'white',
        marginBottom: 20,
        paddingHorizontal: 10,
        paddingLeft: 15,
        paddingRight: 15
    }
});
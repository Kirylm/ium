import axios from 'axios';
import Config from '../Authorization/Config'
import AuthorizationService from "../Authorization/AuthorizationService"
import {BeforSend, AfterSendError} from "../Helpers/AxiosInstanceHelper" 
import NetInfoWrapper from "../Helpers/NetInfoWrapper"
import AsyncStorageWrapper from "../Helpers/AsyncStorageWrapper"
import { DeleteProductFromList, AddProduct, FindProduct, AfterDownoladFromWebApi, UpdateAmount } from '../Helpers/OpearationsOnProduct';
import CreateProduct from '../Components/Home/CreateProduct';

export default class RestCaller {
    constructor(navigate) {
        this.navigating = navigate;
        this.axiosInstanceValue = null;
    }

    static get navigating() {
        this.navigating;
    }

    static get axiosInstance() {
        if (this.axiosInstanceValue) {
            return this.axiosInstanceValue;
        }
        else {
            this.axiosInstanceValue = axios.create();

            this.axiosInstanceValue.interceptors.request.use(BeforSend,
            (error) => {
                return Promise.reject(error);
            });

            this.axiosInstanceValue.interceptors.response.use((response) => {
                return response;
            }, AfterSendError);

            return this.axiosInstanceValue;
        }
    }

    async getProducts() {
        const isConnected = await NetInfoWrapper.isConnected();
        if (isConnected) {
            const data = await this.getProductsOnline();
            return data;
        }
        else {
            const data = await this.getProductsOffline();
            return data;
        }
    }

    async getProductsOnline() {
        try
        {
            const instance = RestCaller.axiosInstance;
            const response = await instance.get(Config.getProductsUrl);
            console.log(response);
            const dataFromMemory = await AsyncStorageWrapper.getProducts();
            const data = AfterDownoladFromWebApi(dataFromMemory, response.data); 
            AsyncStorageWrapper.saveProducts(data);
            return data;
        }
        catch(exception) {
            console.log(exception);
            alert("Error while getting products");
        }
    }

    async getProductsOffline() {
        return await AsyncStorageWrapper.getProducts();
    }

    async clearQueue() {
        const queue = await AsyncStorageWrapper.getStackToSend();
        const products = await AsyncStorageWrapper.getProducts();
        
        await AsyncStorageWrapper.clearStack();

        for (const value of queue) {
            const axiosInstance = RestCaller.axiosInstance;
            console.log(value);
            await axiosInstance(value);
        }

        for (const value of products) {
            if (value.id >= 0) {
                const changeValueModel = {
                    "Id" : value.id,
                    "Amount" : value.yourAmount
                }
                console.log(changeValueModel);
                await this.updateAmount(changeValueModel);
            }
            else {
                const product = {
                    "ProductName" : value.productName,
                    "NameOfShop" : value.nameOfShop,
                    "Price" : value.price,
                    "YourAmount" : value.yourAmount,
                    "Shopes" : value.shopes,
                    "Tag" : value.tag
                };
                console.log(product);
                await this.createProduct(product);
            }
        }
    }

    async updateAmount(increaseModel) {
        try
        {
            const isConnected = await NetInfoWrapper.isConnected();
            await UpdateAmount(increaseModel.Id, increaseModel.Amount);
            if(isConnected) {
                const instance = RestCaller.axiosInstance;
                const url =  Config.changeAmountUrl 
                const response = await instance.patch(url, increaseModel);
                console.log(response);
                return response;
            }
        }
        catch(exception) {
            console.log(exception);
            alert("Error while updating");
        }
    }

    async deleteProduct(id) {
        try {
            await DeleteProductFromList(id);
            if (id >= 0) {
                const instance = RestCaller.axiosInstance;
                const response = await instance.delete(Config.deleteProductUrl + id);
                console.log(response);
                return response;    
            }
        }
        catch(exception) {
            console.log(exception);
            alert("Error while deleting")
        }
    }

    async createProduct(product) {
        try {
            const isConnected = await NetInfoWrapper.isConnected();
            await AddProduct(product);
            if (isConnected)
            {
                const instance = RestCaller.axiosInstance;
                const response = await instance.post(Config.createProductUrl, product);
                console.log(response);
                return response;
            }
        }
        catch(exception) {
            console.log(exception);
            alert("Error while saving element");
        }
    }
}
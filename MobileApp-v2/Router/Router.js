import { StackNavigator } from "react-navigation";
import LoginForm from "../Components/Login/LoginForm"
import SignUpForm from "../Components/Login/SignUpForm"
import Home from "../Components/Home/Home"
import CreateProduct from "../Components/Home/CreateProduct"
import ProductCard from "../Components/Home/ProductCard";

export const LoginUser = StackNavigator({
    Login: {
        screen: LoginForm,
        navigationOptions: {
            title: "Sign In"
        }
    },
    Register: {
        screen: SignUpForm,
        navigationOptions: {
            title: "Sign Up"
        }
    },
});

export const AppView = StackNavigator({
  Home: {
    screen: Home
  },
  CreateProduct: {
    screen: CreateProduct,
    navigationOptions: {
      gesturesEnabled: false,
      title: "Create product"
    }
  },
  ProductCard: {
    screen: ProductCard,
    navigationOptions: {
      gesturesEnabled: false,
      title: "Edit product"
    }
  }
})

export const createRootNavigator = (signedIn = false) => {
    return StackNavigator(
      {
        AppView: {
          screen: AppView,
          navigationOptions: {
            gesturesEnabled: false
          }
        },

        LoginUser: {
          screen: LoginUser,
          navigationOptions: {
            gesturesEnabled: false
          }
        }
      },
      {
        headerMode: "none",
        mode: "modal",
        initialRouteName: signedIn ? "AppView" : "LoginUser"
      }
    );
  };



/**
 * Created by Kiryl on 10/22/2017.
 */
import Config from './Config';
import AsyncStorageWrapper from  './../Helpers/AsyncStorageWrapper';
import axios from 'axios';
import GetDeviceIdWrapper from './../Helpers/GetDeviceIdWrapper'

export default class AuthorizationService {
    
    static async registerUser(data) {
        try {
            const response =  await axios.post(Config.registerUserUrl, data);
            alert("Account created");
            return response;
        }
        catch (exception) {
            if (exception.response.status === 400) {
                console.log(exception);
                alert(exception.response.data.modelState[""][0]); 
            }
            else {
                alert("Error while creating account. Please try later");
            }
        }
    }

    static async refreshToken() {
        const tokenRefresher = await AsyncStorageWrapper.getItem(Config.tokenRefresher);

        const id = await GetDeviceIdWrapper.DeviceId();

        const objectToSend = {
            refresh_token : tokenRefresher,
            grant_type : 'refresh_token',
            client_Id : Config.clientId,
            mobileDeviceId : id
        };

        console.log(objectToSend);

        await this.getToken(objectToSend);
    }

    static async loginUser(user, navigate) {
        try 
        {
            const id = await GetDeviceIdWrapper.DeviceId();

            const objectToSend = {
                grant_type : 'password',
                username : user.userName,
                password : user.password,
                client_Id : Config.clientId,
                mobileDeviceId : id
            };
            await this.getToken(objectToSend);
            navigate("AppView");
        }
        catch (exception) {
            if (exception.response.status === 400) {
                console.log(exception.response.data.error_description);
                alert(exception.response.data.error_description); 
            }
            else {
                console.log(exception);
                alert("Error while sign in. Please try later");
            }
        }
    }

    static async logOut() {
        const keys = [Config.tokenRefresher, Config.token, Config.tokenType, Config.userName];
        await AsyncStorageWrapper.removeMulti(keys);
        await AsyncStorageWrapper.clearStack();
        await AsyncStorageWrapper.saveProducts([]);
    }

    static async isUserLogged() {
        const value = await AsyncStorageWrapper.getItem(Config.tokenRefresher);
        return !!value;
    }

    static async getTokenFromMemory() {
        return await AsyncStorageWrapper.getItem(Config.token);
    }

    static async getRefreshToken() {
        return await AsyncStorageWrapper.getItem(Config.tokenRefresher);
    }

    static async getToken(objectToSend) {
        const queryString = require('query-string');

        var strObject = queryString.stringify(objectToSend);

        const response = await axios.post(Config.tokenUrl, strObject, {headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        }});

        console.log(response);

        if (response.status === 200) {
            await AsyncStorageWrapper.setItem(Config.tokenRefresher, response.data.refresh_token);
            console.log(response.data.refresh_token);
            await AsyncStorageWrapper.setItem(Config.token, response.data.access_token);
            console.log(response.data.access_token);
            await AsyncStorageWrapper.setItem(Config.userName, response.data.userName);
            await AsyncStorageWrapper.setItem(Config.tokenType, response.data.token_type);
        }
    }
}
/**
 * Created by Kiryl on 10/22/2017.
 */
const client_Id  = "IUMApp";
const url = "https://webapi20171023070443.azurewebsites.net/";
const registerUser = "api/Account/Register/";
const token = "token";
const tokenRefresher = "tokenRefresher";
const appName  = "IUMMobileApp";
const userName = "userName";
const tokenType = "tokenType";
const products = "Products";

export default class Config {

    static get clientId() {
        return client_Id;
    }

    static get url() {
        return url;
    }

    static get registerUserUrl() {
        return "https://webapi20171023070443.azurewebsites.net/api/Account/Register/";
    }

    static get tokenUrl() {
        return url + token;
    }

    static get token() {
        return appName + token;
    }

    static get tokenRefresher() {
        return appName + tokenRefresher;
    }

    static get userName() {
        return appName + userName;
    }

    static get tokenType() {
        return appName + tokenType;
    }

    static get getProductsUrl() {
        return "https://webapi20171023070443.azurewebsites.net/api/Products/GetAllProducts/";
    }

    static get increaseAmountUrl() {
        return "https://webapi20171023070443.azurewebsites.net/api/Products/IncreaseAmount/";
    }

    static get decreaseAmountUrl() {
        return "https://webapi20171023070443.azurewebsites.net/api/Products/DecreaseAmount/";
    }

    static get deleteProductUrl() {
        return "https://webapi20171023070443.azurewebsites.net/api/Products/DeleteProduct/";
    }

    static get createProductUrl() {
        return "https://webapi20171023070443.azurewebsites.net/api/Products/CreateProduct/";    
    }

    static get changeAmountUrl() {
        return "https://webapi20171023070443.azurewebsites.net/api/Products/ChangeAmount/";  
    }
}
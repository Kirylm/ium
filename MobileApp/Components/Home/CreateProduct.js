import React, { Component } from 'react'
import { StyleSheet, 
    View, 
    Text, 
    Button, 
    TextInput,
    Keyboard } from 'react-native';
import { Card } from 'react-native-elements'
import { NumericTextChanged, FloatTextChanged } from '../../Helpers/NumericInputHelper'

export default class CreateProduct extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {name: '', 
            amount: '',
            price: '',
            shop: '',
            onClick: params.onClick};
    }

    onAmountChanged(text) {
        NumericTextChanged(text, (amount) => this.setState({amount}));
    }

    onPriceChanged(text) {
        FloatTextChanged(text, (price) => this.setState({price}));
    }

    async submitProduct() {
        Keyboard.dismiss();

        console.log(this.state.amount);

        const product = {
          "ProductName" : this.state.name,
          "NameOfShop" : this.state.shop,
          "Price" : parseFloat(this.state.price),
          "YourAmount" : parseInt(this.state.amount)
        };

        await this.state.onClick(product);
    }

    render() {
        return(
            <View>
                <Card>
                    <TextInput 
                        placeholder = "Name of product"
                        value = {this.state.name}
                        onChangeText = {(name)=> this.setState({name})}
                        returnKeyLabel = "Next"
                        ref = {(input) => this.nameInput = input}
                        onSubmitEditing = {() => {this.shopInput.focus()}}
                    />
                    <TextInput 
                        placeholder = "Name of shop"
                        value = {this.state.shop}
                        onChangeText = {(shop) => this.setState({shop})}
                        returnKeyLabel = "Next"
                        ref = {(input) => this.shopInput = input}
                        onSubmitEditing = {() => {this.amountInput.focus()}}
                    />
                    <TextInput 
                        placeholder = "Amount"
                        value = {this.state.amount}
                        keyboardType = 'numeric'
                        onChangeText = {(amount) => this.onAmountChanged(amount)}
                        returnKeyLabel = "Next"
                        ref = {(input) => this.amountInput = input}
                        onSubmitEditing = {() => {this.priceInput.focus()}}
                    />
                    <TextInput 
                        placeholder = "Price"
                        value = {this.state.price}
                        keyboardType = 'numeric'
                        onChangeText = {(price) => this.onPriceChanged(price)}
                        returnKeyLabel = "Next"
                        ref = {(input) => this.priceInput = input}
                    />
                    <Button 
                        title="Submit" 
                        backgroundColor="#4286f4"
                        disabled={
                            this.state.name.length === 0 ||
                            this.state.shop.length === 0 ||
                            this.state.amount.length === 0 ||
                            this.state.price.length === 0
                        }

                        onPress = {() => this.submitProduct()}
                    />
                </Card>
            </View>
        );
    }
}
/**
 * Created by Kiryl on 10/22/2017.
 */
import { AsyncStorage } from 'react-native'
const stackToSend = "StackToSend";
const products = "Items";

export default class AsyncStorageWrapper {
    static async setStackToSend(config) {
        const stack = await AsyncStorageWrapper.getStackToSend();
        console.log(stack);
        stack.push(config);
        await AsyncStorageWrapper.setItem(stackToSend, JSON.stringify(stack));
    }
    
    static async clearStack() {
        await AsyncStorageWrapper.setItem(stackToSend, JSON.stringify([]));    
    }

    static async getProducts() {
        const data = await AsyncStorageWrapper.getItem(products);
        return JSON.parse(data);
    }

    static async saveProducts(data) {
        return await AsyncStorageWrapper.setItem(products, JSON.stringify(data)); 
    }

    static async getStackToSend() {
        const stack = await AsyncStorageWrapper.getItem(stackToSend);
        console.log(stack);
        if (stack === null)
            return [];
        else 
            return JSON.parse(stack);
    }
    
    static async setItem(key, value) {
        let isSuccess = true;

        try {
            await AsyncStorage.setItem(key, value);
        }
        catch(error) {
            console.log(error)
            isSuccess = false;
        }

        return isSuccess;
    }

    static async getItem(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            return value;
        }
        catch(error) {
            console.log(error)
            return null;
        }
    }

    static async removeItem(key) {
        try {
            await AsyncStorage.removeItem(key);
        }
        catch(error) {
            console.log(error);
        }
    }

    static async removeMulti(keys) {
        try {
            await AsyncStorage.multiRemove(keys);
        }
        catch(error) {
            console.log(error);
        }
    }
}
import AuthorizationService from '../Authorization/AuthorizationService'
import RestCaller from '../RestCaller/RestCaller'
import NetInfoWrapper from './NetInfoWrapper';
import AsyncStorageWrapper from './AsyncStorageWrapper';

async function BeforSend(config) {
    const token = await AuthorizationService.getTokenFromMemory();
    if (token) {
        config.headers['authorization'] = 'Bearer ' + token;
    }
    return config;
}

async function AfterSendError(error) {
    console.log(error);
    if (error.message === "Network Error") {
        await AsyncStorageWrapper.setStackToSend(error.config);
        return Promise.resolve(error);
    }
    else if (401 === error.response.status) {
        try {
            await AuthorizationService.refreshToken();
            console.log(error);
            console.log(RestCaller.axiosInstance);
            return await RestCaller.axiosInstance(error.config);
        }
        catch(errorToken){
            console.log(errorToken);
            if (401 === errorToken.response.status) {
                RestCaller.navigating();
                return Promise.reject(error);
            }
        }
    }
    else {
        return Promise.reject(error);
    }
}

export {BeforSend, AfterSendError}
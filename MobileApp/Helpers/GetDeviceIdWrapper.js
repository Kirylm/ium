export default class GetDeviceIdWrapper {
    static async DeviceId() {
        try {
            const uniqueId = require('react-native-unique-id')
            const id = await uniqueId();
            return id;
        }
        catch(exception) {
            console.log(exception);
            return null;
        }
    }
}
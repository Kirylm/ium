import { NetInfo, BackHandler, Alert } from 'react-native';

export default class NetInfoWrapper {
    static async isConnected() {
        try {
            const isConnected = await NetInfo.isConnected.fetch();
            console.log(isConnected);
            return isConnected;
        }
        catch(exception) {
            console.log(exception);
            return false;
        }
    }

    static addOnChangeEventListner(callOnChange) {
        NetInfo.isConnected.addEventListener('connectionChange', callOnChange);
    }

    static removeEventListner() {
        NetInfo.isConnected.removeEventListener('connectionChange');
    }
    
    static async ifNotConnectedClosed() {
        var isConnected = await NetInfoWrapper.isConnected();
        if (!isConnected) {

            Alert.alert("Error", "You need to be connected to internet", 
            [
                {
                    text: "Ok",
                    onPress: BackHandler.exitApp
                }
            ])           
        }
    }
}
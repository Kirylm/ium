function NumericTextChanged (text, stateChanger) {
    let newText = '';
    const numbers = '0123456789';
  
    for (var i=0; i < text.length; i++) {
         if(numbers.indexOf(text[i]) > -1 ) {
              newText = newText + text[i];
         }
         else {
               alert("please enter numbers only");
          }
         
     }
     
     stateChanger(newText); 
}

function FloatTextChanged (text, stateChanger) {
    const indexOfDot = text.indexOf('.');
    if (indexOfDot === -1 && text.length != 0) {
        const floatValue = parseFloat(text);
        stateChanger(floatValue.toString());
    }
    else if (indexOfDot <= (text.length - 3)) {
        const floatValue = parseFloat(text);
        stateChanger(floatValue.toFixed(2).toString());
    }
    else {
        stateChanger(text);
    }
}

export { NumericTextChanged, FloatTextChanged };
import AsyncStorageWrapper from "./AsyncStorageWrapper";

async function DeleteProductFromList(productId) {
    let products = await AsyncStorageWrapper.getProducts();
    const diff = products.filter(e => e.id !== productId)
    AsyncStorageWrapper.saveProducts(diff);
}

async function AddProduct(product) {
    let products = await AsyncStorageWrapper.getProducts();
    console.log(product.YourAmount);
    const newProduct = { 
        id : -1,
        productName : product.ProductName,
        nameOfShop : product.NameOfShop,
        price : product.Price,
        yourAmount : product.YourAmount,
        otherDevices : null
    }
    
    newProduct.id -= Math.floor((Math.random() * 100) + 1);
    product.id = newProduct.id;
    products.push(newProduct);
    AsyncStorageWrapper.saveProducts(products);
}

function AfterDownoladFromWebApi(fromMemory, fromWebApi) {
    let arrayToReturn = [];
    for (let valueFromWebApi of fromWebApi) {
        // for (let valueFromMemory of fromMemory) {
        //     if (valueFromMemory.id === valueFromMemory.id) {
        //         valueFromWebApi.yourAmount = valueFromMemory.yourAmount;
        //     }
        // }
        arrayToReturn.push(valueFromWebApi);
    }
    return arrayToReturn;
}

async function UpdateAmount(id, amount) {
    let products = await AsyncStorageWrapper.getProducts();
    
    for (let value of products) {
        if (value.id === id) {
            value.yourAmount = amount;
        }
    }

    await AsyncStorageWrapper.saveProducts(products);
}

async function FindProduct(id) {
    const products = await AsyncStorageWrapper.getProducts();
    const finded = products.filter(e => e.id === id);
    return finded;
}

export{DeleteProductFromList, AddProduct, FindProduct, UpdateAmount, AfterDownoladFromWebApi}
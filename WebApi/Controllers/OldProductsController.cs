﻿namespace WebApi.Controllers
{
    using System;
    using System.Data.Entity.Infrastructure;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;
    using DataAccess.Entity;
    using DataAccess.Repository;
    using Helpers;
    using Model;

    [Authorize]
    public class OldProductsController : ApiController
    {
        private readonly IRepository<Product> _productRepository;

        public OldProductsController()
        {
            this._productRepository = new ProductRepository();
        }

        [HttpPost]
        public async Task<IHttpActionResult> CreateProduct(Product product)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            try
            {
                var entity = await this._productRepository.AddAsync(product);
                var location = this.Request.RequestUri + "/" + entity.Id;
                return this.Created(location, entity);
            }
            catch (DbUpdateException ex)
            {
               Trace.WriteLine(ex);
               return this.SendErrorResponse(ex.ToString(), HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString());
            }
        }

        [HttpPatch]
        public async Task<IHttpActionResult> IncreaseAmount(ProductPatchModel model)
        {
            return await this.IncreaseOrDecrease(model, IntOperator.Increase);
        }
        
        [HttpPatch]
        public async Task<IHttpActionResult> DecreaseAmount(ProductPatchModel model)
        {
            return await this.IncreaseOrDecrease(model, IntOperator.Decrease);
        }
        
        [HttpGet]
        public IHttpActionResult GetAllProducts()
        {
            try
            {
                return this.Ok(this._productRepository.GetAll());
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString());
            }
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProduct(int id)
        {
            try
            {
                var element = this._productRepository.GetAll().FirstOrDefault(m => m.Id == id);

                if (element == null)
                {
                    return this.NotFound();
                }

                await this._productRepository.DeleteAsync(id);

                return this.Ok();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString());
            }
        }

        private async Task<IHttpActionResult> IncreaseOrDecrease(ProductPatchModel model, Func<int, int, int> operatorFunc)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            try
            {
                var entity = await this._productRepository.FindByIdAsync(model.Id);
                entity.Amount = operatorFunc(entity.Amount, model.Amount);

                return this.Ok(await this._productRepository.UpdateAsync(entity));
            }
            catch (DbUpdateException ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString(), HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString());
            }
        }

        private IHttpActionResult SendErrorResponse(string message, HttpStatusCode code = HttpStatusCode.InternalServerError)
        {
            return this.ResponseMessage(this.Request.CreateErrorResponse(code, message));
        }
    }
}

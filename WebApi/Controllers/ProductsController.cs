﻿namespace WebApi.Controllers
{
    using System;
    using System.Data.Entity.Infrastructure;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;
    using DataAccess.Models;
    using DataAccess.Repository;
    using Model;

    [Authorize]
    public class ProductsController : ApiController
    {
        private readonly ProductRepository _productRepository;

        public ProductsController()
        {
            this._productRepository = new ProductRepository();
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> CreateProduct(ProductModel product)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            try
            {
                var entity = await this._productRepository.CreateProduct(product);
                var location = this.Request.RequestUri + "/" + entity.Id;
                return this.Created(location, entity);
            }
            catch (DbUpdateException ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString(), HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString());
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetAllProducts()
        {
            try
            {
                return this.Ok(await this._productRepository.GetAllProductModelsAsync());
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString());
            }
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProduct(int id)
        {
            try
            {
                var element = this._productRepository.GetAll().FirstOrDefault(m => m.Id == id);

                if (element == null)
                {
                    return this.Ok();
                }

                await this._productRepository.DeleteAsync(id);

                return this.Ok();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString());
            }
        }

        [HttpPatch]
        public async Task<IHttpActionResult> ChangeAmount(ProductPatchModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var element = this._productRepository.GetAll().FirstOrDefault(m => m.Id == model.Id);

            if (element == null)
            {
                return this.Ok();
            }

            try
            {
                await this._productRepository.UpdateProduct(model.Id, model.Amount);
                return this.Ok();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return this.SendErrorResponse(ex.ToString());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._productRepository.Dispose();
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult SendErrorResponse(string message, HttpStatusCode code = HttpStatusCode.InternalServerError)
        {
            return this.ResponseMessage(this.Request.CreateErrorResponse(code, message));
        }
    }
}

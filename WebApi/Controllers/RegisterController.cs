﻿namespace WebApi.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Http;
    using DataAccess.Models;
    using DataAccess.Repository;
    using Microsoft.AspNet.Identity;


    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private readonly AuthRepository _repo;

        public AccountController()
        {
            this._repo = new AuthRepository();
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(UserModel userModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var result = await this._repo.RegisterUserAsync(userModel);

            var errorResult = this.GetErrorResult(result);

            return errorResult ?? this.Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._repo.Dispose();
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return this.InternalServerError();
            }

            if (result.Succeeded) return null;
            if (result.Errors != null)
            {
                foreach (var error in result.Errors)
                {
                    this.ModelState.AddModelError("", error);
                }
            }

            if (this.ModelState.IsValid)
            {
                return this.BadRequest();
            }

            return this.BadRequest(this.ModelState);
        }
    }
}

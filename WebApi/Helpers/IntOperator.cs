﻿namespace WebApi.Helpers
{
    public static class IntOperator
    {
        public static int Increase(int a, int b)
        {
            return a + b;
        }

        public static int Decrease(int a, int b)
        {
            return a - b;
        }
    }
}
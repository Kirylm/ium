﻿namespace WebApi.Model
{
   using System.ComponentModel.DataAnnotations;

    public class ProductPatchModel
    {
        [Required]
        public int Id { get; set; }
        
        public int Amount { get; set; } 
    }
}